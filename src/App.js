import React from "react";
import "./App.css";
import "antd/dist/antd.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import * as firebase from "firebase";
import HomePage from "./Pages/HomePage/HomePage";
import HistoryPage from "./Pages/HistoryPage/HistoryPage";
import StaffPage from "./Pages/StaffPage/StaffPage";
import StaffDetailPage from "./Pages/StaffDetailPage/StaffDetailPage";
import FormPage from "./Pages/FormPage/FormPage";
import AdminLoginPage from "./Pages/AdminLoginPage/AdminLoginPage";
import ProgramPage from "./Pages/ProgramPage/ProgramPage";
import AdminAddFormPage from "./Pages/AdminAddFormPage/AdminAddFormPage";
import AdminAddStaffPage from "./Pages/AdminAddStaffPage/AdminAddStaffPage";
import AdminFormPage from "./Pages/AdminFormPage/AdminFormPage";
import AdminStaffPage from "./Pages/AdminStaffPage/AdminStaffPage";
import AdminStaffDetailPage from "./Pages/AdminStaffDetailPage/AdminStaffDetailPage";

class App extends React.Component {
    constructor(props) {
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyDxozaS4K4rmK7gjblCZ52T8C4Wf0y2DtU",
            authDomain: "microkmuttweb.firebaseapp.com",
            databaseURL: "https://microkmuttweb.firebaseio.com",
            projectId: "microkmuttweb",
            storageBucket: "microkmuttweb.appspot.com",
            messagingSenderId: "430308354045"
        };
        firebase.initializeApp(config);

        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };

    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen,
        });
    }

    render() {
        return (
            <Router basename="/mic/redesign">
                <Switch>
                    <div className="App">
                        <Route exact path="/" component={HomePage} />
                        <Route
                            path="/administrator"
                            component={AdminLoginPage}
                        />
                        <Route path="/history" component={HistoryPage} />
                        <Route path="/staff" component={StaffPage} />
                        <Route path="/staffdetail/:id" component={StaffDetailPage} />
                        <Route path="/form" component={FormPage} />
                        <Route path="/program" component={ProgramPage} />
                        <Route path="/admin-addform" component={AdminAddFormPage} />
                        <Route path="/admin-form" component={AdminFormPage} />
                        <Route path="/admin-addstaff" component={AdminAddStaffPage} />
                        <Route path="/admin-staff" component={AdminStaffPage} />
                        <Route path="/admin-staffdetail/:id" component={AdminStaffDetailPage} />
                        
                    </div>
                </Switch>
            </Router>
        );
    }
}

export default App;
