import React, { Component } from "react";
import { Layout, Menu, Breadcrumb, Row, Icon, message } from "antd";
import "./AdminHeaderComponent.css";
import { BrowserRouter as Router, Link, Route, Switch, withRouter } from "react-router-dom";
import kmutt from "../../assets/kmutt.png";
import * as firebase from 'firebase';

const { Header, Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class AdminHeaderComponent extends React.Component {
    constructor() {
        super();

        this.state = {
            collapsed: false,
            user: null
        };

        let fbuser = firebase.auth().onAuthStateChanged(user => {
            if (user) {
                this.setState({
                    user: user
                })
            }  else {
                fbuser()
                this.props.history.push("/administrator");
            }
        })
    }

    componentDidMount() {
        
    }

    logout = (e) => {
        firebase.auth().signOut().then(() => {
            message.success("ออกจากระบบ")
            e.preventDefault();
        }).catch(err => {
            console.error("Signout error");
        })
    }

    render() {
        return (
            <div>
                <Row className="header-bar">
                    <span>
                        <img className="header-img" src={kmutt} />
                    </span>
                    <div className="header-title">
                        <Row className="header-title-micro">ภาควิชาจุลชีววิทยา</Row>
                        <Row>
                            คณะวิทยาศาสตร์ มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี
                        </Row>
                    </div>

                    {this.state.user ? <span className="header-logout" style={{ marginLeft: "auto" }}> <a onClick={this.logout}> ออกจากระบบ </a></span> : ""}

                    <span
                        className="header-hamburger"
                        style={{ marginLeft: "auto" }}
                    >
                        <Icon
                            type="bars"
                            style={{ fontSize: "32px", right: "300px" }}
                            onClick={() => {
                                this.setState({
                                    collapsed: !this.state.collapsed
                                });
                            }}
                        />
                    </span>
                </Row>

                {this.state.collapsed ? (
                    <div className="hamburger-menu">
                        <Menu
                            mode="inline"
                            style={{
                                height: "100%",
                                borderRight: 0,
                                background: "#ffffff"
                            }}
                        >

                            <Menu.Item key="staff">
                                <Link to={`/admin-staff`}>
                                    <Icon type="user" />บุคลากร
                                </Link>
                            </Menu.Item>

                            <Menu.Item key="form">
                                <Link to={`/admin-form`}>
                                    <Icon type="form" />แบบฟอร์มต่างๆ
                                </Link>
                            </Menu.Item>

                            <Menu.Item key="logout" >
                                <div onClick={this.logout} style={{ width: "100%"}}>ออกจากระบบ</div>
                            </Menu.Item>
                        </Menu>
                    </div>
                ) : (
                        ""
                    )}

                <Header
                    style={{
                        position: "fixed",
                        width: "100%",
                        padding: "0 20px",
                        top: 70
                    }}
                >
                    {/* <div className="logo" /> */}
                    <Menu
                        // theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={["1"]}
                        style={{ lineHeight: "48px" }}
                    >
                        <Menu.Item key="staff">
                            <Link to={`/admin-staff`}>บุคลากร</Link>
                        </Menu.Item>

                        <Menu.Item key="form">
                            <Link to={`/admin-form`}>แบบฟอร์มต่างๆ</Link>
                        </Menu.Item>


                    </Menu>
                </Header>
            </div>
        );
    }
}

export default withRouter(AdminHeaderComponent);
