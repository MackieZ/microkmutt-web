import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./AdminStaffComponent.css";
import { Card, Col, Row, Button, Icon, Modal, message } from "antd";
import manuser from "../../assets/man-user.png";
const confirm = Modal.confirm;
const { Meta } = Card;

class AdminStaffComponent extends Component {

    state = {
        staff: []
    };

    componentDidMount() {
        const fd = new FormData();
        fetch("http://202.44.11.244/mic/api/?m=load_staff_list", {
            method: "POST",
            body: fd
        })
            .then(res => res.json())
            .then(resJson => {
                let st = resJson.list;
                resJson.list.forEach((staff, index) => {
                    if (staff.position === "หัวหน้าภาควิชา")
                        st[index]['position_number'] = 1;
                    if (staff.position === "รองหัวหน้าภาควิชา")
                        st[index]['position_number'] = 2;
                    if (staff.position === "อาจารย์")
                        st[index]['position_number'] = 3;
                    if (staff.position === "นักวิจัย")
                        st[index]['position_number'] = 4;
                    if (staff.position === "หัวหน้าห้องปฏิบัติการ")
                        st[index]['position_number'] = 5;
                    if (staff.position === "เจ้าหน้าที่")
                        st[index]['position_number'] = 6;
                })
                st.sort(
                    (a, b) => { return a.position_number - b.position_number }
                )
                this.setState({ staff: st });
            });
    }

    handleDelete = id => {
        confirm({
            title: "ต้องการลบบุคลากร ใช่หรือไม่?",
            content: "หากลบไปแล้วจะไม่สามารถนำกลับมาได้",
            okText: "ใช่",
            okType: "danger",
            cancelText: "ยกเลิก",
            onOk() {
                const fd = new FormData();
                fd.append("id", id);
                fetch("http://202.44.11.244/mic/api/?m=delete_staff", {
                    method: "POST",
                    body: fd
                })
                    .then(resDel => resDel.json())
                    .then(resDelJson => {
                        if (!resDelJson.is_error) {
                            message.success("ลบบุคลากรสำเร็จ");
                            setTimeout(() => {
                                window.location.reload();
                            }, 1500);
                        }
                    })
                    .catch(err => {
                        console.error(err);
                        message.error(
                            "เกิดข้อผิดพลาดในการเชื่อมต่ออินเตอร์เน็ต"
                        );
                    });
            },
            onCancel() {
            }
        });
    };

    render() {
        return (
            <div className="pad-content">
                <Row>
                    {this.state.staff.map(staffDetails => {
                        return (
                            <Col lg={8} md={12} sm={12} xs={24}>
                                <div className="staff-box">
                                    <Link
                                        to={`/admin-staffdetail/${staffDetails.id}`}
                                    >
                                        <Card
                                            hoverable
                                            cover={
                                                <img
                                                    alt="example"
                                                    src={
                                                        staffDetails.img_url
                                                            ? `http://202.44.11.244/mic/api/${
                                                            staffDetails.img_url
                                                            }`
                                                            : manuser
                                                    }
                                                />
                                            }
                                        >
                                            <Meta
                                                title={
                                                    staffDetails.name_thai
                                                        ? staffDetails.name_thai
                                                        : "-"
                                                }
                                                description={
                                                    staffDetails.position
                                                        ? staffDetails.position
                                                        : "-"
                                                }
                                            />
                                        </Card>
                                    </Link>
                                    <Link to={{ pathname: `/admin-addstaff`, state: { id: staffDetails.id } }}>
                                        <Button id="edit">
                                            <Icon type="edit" />
                                        </Button>
                                    </Link>
                                    <Button
                                        id="delete"
                                        onClick={() =>
                                            this.handleDelete(staffDetails.id)
                                        }
                                    >
                                        <Icon type="delete" />
                                    </Button>
                                </div>
                            </Col>
                        );
                    })}
                </Row>
            </div>
        );
    }
}

export default AdminStaffComponent;
