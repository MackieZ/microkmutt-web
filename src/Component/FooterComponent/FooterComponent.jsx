import React, { Component } from "react";
import { Layout, Menu, Breadcrumb, Row, Col } from "antd";
import "./FooterComponent.css";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import footerphone from "../../assets/footerphone.png";
import footerfax from "../../assets/footerfax.png";
import footeroffice from "../../assets/footeroffice.png";

const { Header, Content, Footer } = Layout;
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class FooterComponent extends React.Component {
    render() {
        return (
            <Footer style={{ textAlign: "center" }}>
                <Row id="footer" >
                    <Col md={24}>
                        <Col><b>ติดต่อภาควิชา</b></Col>
                        <Col md={24}><img src={footerphone} alt="logo" width="20px" height="20px" />
                            &nbsp; 02-470-8887 &nbsp;
                        <img src={footerfax} alt="logo" width="20px" height="20px" />
                            &nbsp; 02-470-8891 </Col>

                    </Col>
                    <Col md={24}><img src={footeroffice} alt="logo" width="20px" height="20px" />
                        &nbsp; 126 ภาควิชาจุลชีววิทยา คณะวิทยาศาสตร์
                        มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี
                    ถนนประชาอุทิศ แขวงบางมด เขตทุ่งครุ กทม. 10140 </Col>
                </Row>
            </Footer>
        );
    }
}

export default FooterComponent;
