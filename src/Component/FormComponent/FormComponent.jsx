import React, { Component } from "react";
import { Table, Row } from "antd";
import "./FormComponent.css";
import pdf from "../../assets/pdf.png";

const columns = [
    {
        title: "ฟอร์ม",
        dataIndex: "name",
        key: "name",
        width: 350,
        render: text => <a href={`http://science.kmutt.ac.th/mic/api/${text.link}`}>{text.name}</a>
    },
    {
        title: "ดาวน์โหลด",
        dataIndex: "link",
        key: "action",
        width: 120,
        render: (text) => (
            <Row>
                <div className="icon">
                    <a href={`http://science.kmutt.ac.th/mic/api/${text}`}>
                        <img
                            src={pdf}
                            alt="pdf"
                            width="20px"
                            height="20px"
                        />
                    </a>
                </div>
            </Row>
        )
    }
];

class FormComponent extends Component {
    state = {
        data: []
    }

    componentDidMount() {
        fetch("http://202.44.11.244/mic/api/?m=load_form_list")
            .then(res => res.json())
            .then(resJson => {
                if (resJson.list) {
                    resJson.list.forEach(element => {
                        this.setState({
                            data: [...this.state.data, {
                                key: element.field.id,
                                name: {
                                    name : element.field.name,
                                    link : element.field.path
                                },
                                link: element.field.path
                            }]
                        })
                    });
                }
            })
    }

    render() {
        return (
            <div id="form-component">
                <div className="pad-before-content" />
                <div className="pad-content">
                    <Table columns={columns} dataSource={this.state.data} />
                </div>
            </div>
        );
    }
}

export default FormComponent;
