import React, { Component } from "react";
import { Layout, Menu, Breadcrumb, Row, Icon } from "antd";
import "./HeaderComponent.css";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import kmutt from "../../assets/kmutt.png";

const { Header, Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class HeaderComponent extends React.Component {
    constructor() {
        super();

        this.state = {
            collapsed: false
        };
    }
    render() {
        return (
            <div>
                <Row className="header-bar">
                    <span>
                        <img className="header-img" src={kmutt} />
                    </span>
                    <div className="header-title">
                        <Row className="header-title-micro">ภาควิชาจุลชีววิทยา</Row>
                        <Row>
                            คณะวิทยาศาสตร์ มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี
                        </Row>
                    </div>

                    <span
                        className="header-hamburger"
                        style={{ marginLeft: "auto" }}
                    >
                        {!this.state.collapsed ? <Icon
                            type="bars"
                            style={{ fontSize: "32px", right: "300px" }}
                            onClick={() => {
                                this.setState({
                                    collapsed: !this.state.collapsed
                                });
                            }}
                        /> : <div style={{ paddingLeft: "30px", fontSize: "22px", right: "270px" }} onClick={() => {
                            this.setState({
                                collapsed: !this.state.collapsed
                            });
                        }}>ปิด</div>}
                    </span>
                </Row>

                {this.state.collapsed ? (
                    <div className="hamburger-menu">
                        <Menu
                            mode="inline"
                            style={{
                                height: "100%",
                                borderRight: 0,
                                background: "#ffffff"
                            }}
                        >
                            <Menu.Item key="home">
                                <Link to={`/`}>
                                    <Icon type="home" />หน้าแรก
                                </Link>
                            </Menu.Item>

                            <Menu.Item key="history">
                                <Link to={`/history`}>
                                    <Icon type="profile" />ประวัติภาควิชา
                                </Link>
                            </Menu.Item>

                            <Menu.Item key="staff">
                                <Link to={`/staff`}>
                                    <Icon type="user" />บุคลากร
                                </Link>
                            </Menu.Item>

                            <Menu.Item key="enroll">
                                <a href="http://admission.kmutt.ac.th/">
                                    <Icon type="star" />การสมัครเข้าศึกษา
                                </a>
                            </Menu.Item>

                            <Menu.Item key="program">
                                <Link to={`/program`}>
                                    <Icon type="tags" />หลักสูตร
                                </Link>
                            </Menu.Item>

                            <SubMenu title={<span>งานวิจัย</span>}>
                                <Menu.Item key="reserch:1">
                                    <a href="http://science.kmutt.ac.th/mic/index.php/2012-12-16-19-17-26/2012-12-18-18-33-53">
                                        Agricultural and Applied Microbiology
                                    </a>
                                </Menu.Item>
                                <Menu.Item key="research:2">
                                    <a href="http://science.kmutt.ac.th/mic/index.php/2012-12-16-19-17-26/2012-12-18-18-34-25">
                                        Food Quality Evaluation and Food Product
                                        Development
                                    </a>
                                </Menu.Item>
                                <Menu.Item key="research:3">
                                    <a href="http://science.kmutt.ac.th/mic/index.php/2012-12-16-19-17-26/2012-12-18-18-34-44">
                                        Food Safety and Quality Improvement
                                    </a>
                                </Menu.Item>
                                <Menu.Item key="research:4">
                                    <a href="http://science.kmutt.ac.th/mic/index.php/2012-12-16-19-17-26/2012-12-18-18-35-00">
                                        Molecular Biology
                                    </a>
                                </Menu.Item>
                            </SubMenu>

                            <SubMenu title={<span>งานการค้า/บริการ</span>}>
                                <Menu.Item key="service:1">
                                    <a href="http://science.kmutt.ac.th/mic/index.php/2012-12-16-19-18-53/2013-01-20-17-10-20">
                                        รายชื่อเครื่องมือวิเคราะห์
                                    </a>
                                </Menu.Item>
                                <Menu.Item key="service:2">
                                    <a href="http://science.kmutt.ac.th/mic/index.php/2012-12-16-19-18-53/2013-01-20-17-10-21">
                                        อัตราค่าบริการ
                                    </a>
                                </Menu.Item>
                            </SubMenu>

                            <Menu.Item key="form">
                                <Link to={`/form`}>
                                    <Icon type="form" />แบบฟอร์มต่างๆ
                                </Link>
                            </Menu.Item>
                        </Menu>
                    </div>
                ) : (
                        ""
                    )}

                <Header
                    style={{
                        position: "fixed",
                        width: "100%",
                        padding: "0 20px",
                        top: 70
                    }}
                >
                    {/* <div className="logo" /> */}
                    <Menu
                        // theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={["1"]}
                        style={{ lineHeight: "48px" }}
                    >
                        <Menu.Item key="home">
                            <Link to={`/`}>หน้าแรก</Link>
                        </Menu.Item>

                        <Menu.Item key="history">
                            <Link to={`/history`}>ประวัติภาควิชา</Link>
                        </Menu.Item>

                        <Menu.Item key="staff">
                            <Link to={`/staff`}>บุคลากร</Link>
                        </Menu.Item>

                        <Menu.Item key="enroll">
                            <a href="http://admission.kmutt.ac.th/">
                                การสมัครเข้าศึกษา
                            </a>
                        </Menu.Item>

                        <Menu.Item key="program">
                            <Link to={`/program`}>หลักสูตร</Link>
                        </Menu.Item>

                        <SubMenu title={<span>งานวิจัย</span>}>
                            <Menu.Item key="reserch:1">
                                <a href="http://science.kmutt.ac.th/mic/index.php/2012-12-16-19-17-26/2012-12-18-18-33-53">
                                    Agricultural and Applied Microbiology
                                </a>
                            </Menu.Item>
                            <Menu.Item key="research:2">
                                <a href="http://science.kmutt.ac.th/mic/index.php/2012-12-16-19-17-26/2012-12-18-18-34-25">
                                    Food Quality Evaluation and Food Product
                                    Development
                                </a>
                            </Menu.Item>
                            <Menu.Item key="research:3">
                                <a href="http://science.kmutt.ac.th/mic/index.php/2012-12-16-19-17-26/2012-12-18-18-34-44">
                                    Food Safety and Quality Improvement
                                </a>
                            </Menu.Item>
                            <Menu.Item key="research:4">
                                <a href="http://science.kmutt.ac.th/mic/index.php/2012-12-16-19-17-26/2012-12-18-18-35-00">
                                    Molecular Biology
                                </a>
                            </Menu.Item>
                        </SubMenu>

                        <SubMenu title={<span>งานการค้า/บริการ</span>}>
                            <Menu.Item key="service:1">
                                <a href="http://science.kmutt.ac.th/mic/index.php/2012-12-16-19-18-53/2013-01-20-17-10-20">
                                    รายชื่อเครื่องมือวิเคราะห์
                                </a>
                            </Menu.Item>
                            <Menu.Item key="service:2">
                                <a href="http://science.kmutt.ac.th/mic/index.php/2012-12-16-19-18-53/2013-01-20-17-10-21">
                                    อัตราค่าบริการ
                                </a>
                            </Menu.Item>
                        </SubMenu>

                        <Menu.Item key="form">
                            <Link to={`/form`}>แบบฟอร์มต่างๆ</Link>
                        </Menu.Item>
                    </Menu>
                </Header>
            </div>
        );
    }
}

export default HeaderComponent;
