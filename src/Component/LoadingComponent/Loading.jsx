import React from "react";

class Loading extends React.Component {
    constructor() {
        super()

        this.state = {
            loading: false
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                loading: false
            });
            this.forceUpdate();
        }, 100);

    }
    
    componentWillMount() {
        window.scrollTo(0, 0)
    }

    render() {
        const { loading } = this.state
        

        if (loading)
            return (
                <div className="loader-bg">
                    <div className="loader" />
                </div>
            );
        return <div></div>;
    }
}

export default Loading;
