import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./StaffComponent.css";
import { Card, Col, Row } from "antd";
import manuser from "../../assets/man-user.png";

const { Meta } = Card;

class StaffComponent extends Component {
    state = {
        staff: []
    };

    componentDidMount() {
        const fd = new FormData();
        fetch("http://202.44.11.244/mic/api/?m=load_staff_list", {
            method: "POST",
            body: fd
        })
            .then(res => res.json())
            .then(resJson => {
                let st = resJson.list;
                resJson.list.forEach((staff, index) => {
                    if (staff.position === "หัวหน้าภาควิชา")
                        st[index]["position_number"] = 1;
                    if (staff.position === "รองหัวหน้าภาควิชา")
                        st[index]["position_number"] = 2;
                    if (staff.position === "อาจารย์")
                        st[index]["position_number"] = 3;
                    if (staff.position === "นักวิจัย")
                        st[index]["position_number"] = 4;
                    if (staff.position === "หัวหน้าห้องปฏิบัติการ")
                        st[index]["position_number"] = 5;
                    if (staff.position === "เจ้าหน้าที่")
                        st[index]["position_number"] = 6;
                });
                st.sort((a, b) => {
                    return a.position_number - b.position_number;
                });
                this.setState({ staff: st });
            });
    }

    render() {
        return (
            <div className="pad-content">
                <Row>
                    {this.state.staff.map(staffDetails => {
                        return (
                            <Col lg={8} md={12} sm={12} xs={24}>
                                <div className="staff-box">
                                    <Link
                                        to={`/staffdetail/${staffDetails.id}`}
                                    >
                                        <Card
                                            hoverable
                                            cover={
                                                <img
                                                    alt="example"
                                                    src={
                                                        staffDetails.img_url
                                                            ? `http://202.44.11.244/mic/api/${
                                                                  staffDetails.img_url
                                                              }`
                                                            : manuser
                                                    }
                                                />
                                            }
                                        >
                                            <Meta
                                                key={staffDetails.id}
                                                title={
                                                    staffDetails.name_thai
                                                        ? staffDetails.name_thai
                                                        : "-"
                                                }
                                                description={
                                                    staffDetails.position
                                                        ? staffDetails.position
                                                        : "-"
                                                }
                                            />
                                        </Card>
                                    </Link>
                                </div>
                            </Col>
                        );
                    })}
                </Row>
            </div>
        );
    }
}

export default StaffComponent;
