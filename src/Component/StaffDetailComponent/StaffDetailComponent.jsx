import React, { Component } from "react";
import { Row } from "antd";
import "./StaffDetailComponent.css";
import manuser from "../../assets/man-user.png";
import staffposition from "../../assets/staffposition.png";
import staffemail from "../../assets/staffemail.png";
import staffphone from "../../assets/staffphone.png";

class StaffDetailComponent extends Component {
    constructor() {
        super();
    }

    state = {
        staff: ""
    };

    componentDidMount() {
        const fd = new FormData();
        fd.append("id", this.props.id);
        fetch("http://202.44.11.244/mic/api/?m=load_staff_detail", {
            method: "POST",
            body: fd
        })
            .then(res => res.json())
            .then(resJson => {
                this.setState({ staff: resJson.obj.field });
            });
    }

    render() {
        const {
            name_thai,
            name_eng,
            type,
            position,
            tel,
            email,
            edu_detail,
            exp_detail,
            research_scope,
            research_done,
            research_doing,
            research_publish,
            award,
            img_url
        } = this.state.staff;

        return (
            <div>
                <br />
                <div className="staff-info">
                    <div>
                        <img
                            className="staff-img"
                            src={
                                img_url
                                    ? `http://202.44.11.244/mic/api/${img_url}`
                                    : manuser
                            }
                            height="auto"
                        />
                    </div>
                    <div className="staff-contact">
                        <div className="staff-name">{name_thai}</div>
                        <div className="staff-name">
                            {name_eng ? `(${name_eng})` : ""}
                        </div>
                        <b>{position}</b>
                        <br />
                        <hr />
                        <img
                            src={staffposition}
                            alt="logo"
                            width="20px"
                            height="20px"
                        />
                        &nbsp; {type ? `${type}` : "-"}
                        <br />
                        <img
                            src={staffemail}
                            alt="logo"
                            width="20px"
                            height="20px"
                        />
                        &nbsp; {email ? `${email}` : "-"} <br />
                        <img
                            src={staffphone}
                            alt="logo"
                            width="20px"
                            height="20px"
                        />
                        &nbsp; {tel ? `${tel}` : "-"}
                        <br />
                    </div>
                </div>
                <div className="staff-detail">
                    <Row>
                        <b id="topic">ข้อมูลการศึกษา</b>
                        <br />
                        <div id="detail">
                            <p className="pre-line">
                                {edu_detail ? `${edu_detail}` : "-"}
                            </p>
                        </div>
                    </Row>
                    <Row id="add-pad">
                        <b id="topic">ประสบการณ์ทำงาน</b>
                        <br />
                        <div id="detail">
                            <p className="pre-line">
                                {exp_detail ? `${exp_detail}` : "-"}
                            </p>
                        </div>
                    </Row>
                    <Row id="add-pad">
                        <b id="topic">ขอบเขตงานวิจัย</b>
                        <br />
                        <div id="detail">
                            <p className="pre-line">
                                {research_scope ? `${research_scope}` : "-"}
                            </p>
                        </div>
                    </Row>
                    <Row id="add-pad">
                        <b id="topic">งานวิจัยที่ผ่านมา</b>
                        <br />
                        <div id="detail">
                            <p className="pre-line">
                                {research_done ? `${research_done}` : "-"}
                            </p>
                        </div>
                    </Row>
                    <Row id="add-pad">
                        <b id="topic">งานวิจัยที่กำลังศึกษา</b>
                        <br />
                        <div id="detail">
                            <p className="pre-line">
                                {research_doing ? `${research_doing}` : "-"}
                            </p>
                        </div>
                    </Row>
                    <Row id="add-pad">
                        <b id="topic">งานวิจัยที่ตีพิมพ์</b>
                        <br />
                        <div id="detail">
                            <p className="pre-line">
                                {research_publish ? `${research_publish}` : "-"}
                            </p>
                        </div>
                    </Row>
                    <Row id="add-pad">
                        <b id="topic">รางวัลที่ได้รับ</b>
                        <br />
                        <p className="pre-line">
                            <div id="detail">{award ? `${award}` : "-"}</div>
                        </p>
                    </Row>
                </div>
            </div>
        );
    }
}

export default StaffDetailComponent;
