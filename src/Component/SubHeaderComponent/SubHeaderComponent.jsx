import React from "react";
import "./SubHeaderComponent.css";
import { Row } from "antd";

class SubHeaderComponent extends React.Component {
    render() {
        return (
            <Row className="sub-header">
                <div>{this.props.title}</div>
            </Row>
        );
    }
}

export default SubHeaderComponent;
