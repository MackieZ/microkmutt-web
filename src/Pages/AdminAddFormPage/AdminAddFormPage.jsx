import React from "react";
import "./AdminAddFormPage.css";
import {
    Form,
    Layout,
    Button,
    Icon,
    Input,
    Progress,
    message
} from "antd";
import AdminHeaderComponent from "../../Component/AdminHeaderComponent/AdminHeaderComponent";
import FooterComponent from "../../Component/FooterComponent/FooterComponent";
import Loading from "../../Component/LoadingComponent/Loading";

const FormItem = Form.Item;

const { Content } = Layout;

class AdminAddFormPage extends React.Component {
    constructor() {
        super();

        this.state = {
            selectedFile: null,
            progress: 0,
            path: "",
            name: ""
        };

    }

    fileSelectedHandler = event => {
        this.setState(
            {
                selectedFile: event.target.files[0],
                progress: 1,
                path: this.state.path,
                name: this.state.name
            },
            () => {
                if(this.state.selectedFile)
                    this.fileUploadHandler();
            }
        );
    };

    fileUploadHandler = () => {
        const fd = new FormData();
        fd.append("file", this.state.selectedFile);
        fd.append("label", "UploadFileForm");

        fetch("http://202.44.11.244/mic/api/?m=upload", {
            method: "POST",
            body: fd
        })
            .then(res => res.text())
            .then(resText => {
                this.setState({
                    selectedFile: this.state.selectedFile,
                    progress: this.state.progress,
                    path: resText,
                    name: this.state.name
                });

                for (var i = 0; i <= 100; i++) {
                    setTimeout(() => {
                        this.setState({
                            selectedFile: this.state.selectedFile,
                            progress: i,
                            path: this.state.path,
                            name: this.state.name
                        });
                    }, i * 100);
                }
            })
            .catch(err => console.error(err));
    };

    handleInput = e => {
        this.setState({
            selectedFile: this.state.selectedFile,
            progress: this.state.progress,
            path: this.state.path,
            name: e.target.value
        });
    };

    addForm = () => {
        const fd = new FormData();
        fd.append("path", this.state.path);
        fd.append("name", this.state.name);
        fetch("http://202.44.11.244/mic/api/?m=save_form", {
            method: "POST",
            body: fd
        })
            .then(res => res.json())
            .then(resJson => {
                if (!resJson.is_error) {
                    message.success("เพิ่มแบบฟอร์มสำเร็จ");
                    this.props.history.push('/Admin-form')
                }
                else message.error("กรุณากรอกข้อมูลให้ครบถ้วน");
            })
            .catch(err => {
                console.error(err);
                message.error("เกิดข้อผิดพลาดในการเชื่อมต่ออินเตอร์เน็ต");
            });
    };

    render() {
        const formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 18 }
        };

        return (
            <Layout>
                <Loading />
                <AdminHeaderComponent />
                <Content>
                    <div id="admin-addform">
                        <div className="form">
                            <div className="title">เพิ่มแบบฟอร์ม</div>
                            <hr />
                            <Form>
                                <FormItem label="อัพโหลด" {...formItemLayout}>
                                    <input
                                        style={{ display: "none" }}
                                        type="file"
                                        onChange={this.fileSelectedHandler}
                                        accept="application/pdf"
                                        ref={fileInput =>
                                            (this.fileInput = fileInput)
                                        }
                                    />
                                    <Button
                                        onClick={() => this.fileInput.click()}
                                    >
                                        <Icon type="upload" /> เลือกไฟล์
                                    </Button>
                                    <span style={{ marginLeft: "5px" }}>
                                        {this.state.selectedFile
                                            ? this.state.selectedFile.name
                                            : ""}
                                    </span>
                                    {this.state.progress != 0 ? (
                                        <Progress
                                            percent={this.state.progress}
                                        />
                                    ) : (
                                            ""
                                        )}
                                    <div
                                        style={{
                                            color: "brown",
                                            font: "12px"
                                        }}
                                    >
                                        {" "}
                                        Support only .pdf text file size less than 10 MB.
                                    </div>
                                </FormItem>

                                <FormItem
                                    label="ตั้งชื่อไฟล์"
                                    {...formItemLayout}
                                >
                                    <Input
                                        placeholder="แบบฟอร์มปฏิบัติงาน"
                                        onChange={this.handleInput}
                                    />
                                </FormItem>
                            </Form>
                            <Button
                                className="form-button"
                                type="primary"
                                onClick={this.addForm}
                            >
                                {" "}
                                เพิ่มแบบฟอร์ม{" "}
                            </Button>
                        </div>
                    </div>
                </Content>
                <FooterComponent />
            </Layout>
        );
    }
}

export default AdminAddFormPage;
