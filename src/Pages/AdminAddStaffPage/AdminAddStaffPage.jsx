import React from "react";
import "./AdminAddStaffPage.css";
import {
    Form,
    Layout,
    Upload,
    Button,
    Icon,
    Input,
    Select,
    message,
    Modal
} from "antd";
import AdminHeaderComponent from "../../Component/AdminHeaderComponent/AdminHeaderComponent";
import FooterComponent from "../../Component/FooterComponent/FooterComponent";
import Loading from "../../Component/LoadingComponent/Loading";

const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

const { Content } = Layout;

class AdminAddStaffPage extends React.Component {

    componentDidMount() {
        if (this.props.location.state) {
            const id = this.props.location.state.id;
            this.setState({ id: id });
            const fd = new FormData();
            fd.append("id", id);
            fetch("http://202.44.11.244/mic/api/?m=load_staff_detail", {
                method: "POST",
                body: fd
            })
                .then(resJson => resJson.json())
                .then(res => {
                    const obj = res.obj.field;
                    this.setState(
                        {
                            ...obj,
                            imgUrl: obj.img_url,
                            name: obj.name_thai
                        },
                        () => {
                            if (this.state.imgUrl) {
                                this.setState({
                                    fileList: [
                                        {
                                            uid: -1,
                                            name: "xxx.png",
                                            status: "done",
                                            url: `http://202.44.11.244/mic/api/${
                                                obj.img_url
                                            }`
                                        }
                                    ]
                                });
                            }
                        }
                    );
                })
                .catch(err => {
                    console.error(err);
                    message.error("เกิดข้อผิดพลาดในการเชื่อมต่ออินเตอร์เน็ต");
                });
        }
    }

    state = {
        previewVisible: false,
        previewImage: "",
        fileList: [],
        name: "",
        name_eng: "",
        type: "",
        position: "",
        tel: "",
        email: "",
        edu_detail: "",
        exp_detail: "",
        research_scope: "",
        research_done: "",
        research_doing: "",
        research_publish: "",
        award: "",
        imgUrl: "",
        id: "",
        changeFile: false,
        loading: false
    };

    handleNameChange = e => this.setState({ name: e.target.value });

    handleNameEngChange = e => this.setState({ name_eng: e.target.value });

    handleTypeChange = value => this.setState({ type: value });

    handlePositionChange = value => this.setState({ position: value });

    handleTelChange = e => this.setState({ tel: e.target.value });

    handleEmailChange = e => this.setState({ email: e.target.value });

    handleEduDetailChange = e => this.setState({ edu_detail: e.target.value });

    handleExpDetailChange = e => this.setState({ exp_detail: e.target.value });

    handleResearchScopeChange = e =>
        this.setState({ research_scope: e.target.value });

    handleResearchDoneChange = e =>
        this.setState({ research_done: e.target.value });

    handleResearchDoingChange = e =>
        this.setState({ research_doing: e.target.value });

    handleResearchPublishChange = e =>
        this.setState({ research_publish: e.target.value });

    handleAwardChange = e => this.setState({ award: e.target.value });

    handleSubmit = () => {
        this.setState({loading: true})
        if (this.state.fileList.length && this.state.changeFile) {
            this.uploadImg();
        } else {
            this.saveDetail();
        }
    };

    saveDetail = () => {
        const {
            previewVisible,
            previewImage,
            fileList,
            name,
            name_eng,
            type,
            position,
            tel,
            email,
            edu_detail,
            exp_detail,
            research_scope,
            research_done,
            research_doing,
            research_publish,
            award,
            imgUrl,
            id,
            changeFile
        } = this.state;
        const fd = new FormData();
        if (this.state.id) fd.append("id", id); // ถ้าเป็น edit ให้ใส่ id มาด้วย
        if (changeFile && fileList.length == 0) fd.append("img_url", "");
        else fd.append("img_url", imgUrl);
        fd.append("name_thai", name);
        fd.append("name_eng", name_eng);
        fd.append("type", type);
        fd.append("position", position);
        fd.append("tel", tel);
        fd.append("email", email);
        fd.append("edu_detail", edu_detail);
        fd.append("exp_detail", exp_detail);
        fd.append("research_scope", research_scope);
        fd.append("research_done", research_done);
        fd.append("research_doing", research_doing);
        fd.append("research_publish", research_publish);
        fd.append("award", award);
        fetch("http://202.44.11.244/mic/api/?m=save_staff", {
            method: "POST",
            body: fd
        })
            .then(res => res.json())
            .then(resJson => {
                message.success(
                    !this.state.id ? "เพิ่มบุคลากรสำเร็จ" : "แก้ไขบุคลากร"
                );
                this.props.history.push("/admin-staff");
            })
            .catch(err => {
                console.error(err);
                message.error("เกิดข้อผิดพลาดในการเชื่อมต่ออินเตอร์เน็ต");
            });
    };

    uploadImg = () => {
        const fd = new FormData();
        fd.append("file", this.state.fileList[0].originFileObj);
        fd.append("label", "files");
        fetch("http://202.44.11.244/mic/api/?m=upload_staff_img", {
            method: "POST",
            body: fd
        })
            .then(imgUrl => imgUrl.text())
            .then(url => {
                this.setState({ imgUrl: url }, () => this.saveDetail());
            })
            .catch(err => {
                console.error(err);
                message.error("เกิดข้อผิดพลาดในการเชื่อมต่ออินเตอร์เน็ต");
            });
    };

    handleCancel = () => this.setState({ previewVisible: false });

    handlePreview = file => {
        this.setState({
            previewImage: file.url || file.thumbUrl,
            previewVisible: true
        });
    };

    handleChange = ({ fileList }) =>
        this.setState({ fileList }, () => {
            let fileProp = [...this.state.fileList];
            if (fileProp.length > 0) fileProp[0].status = "done";
            this.setState({ fileList: fileProp, changeFile: true });
        });

    render() {
        const formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 18 }
        };
        const { previewVisible, previewImage, fileList } = this.state;
        const uploadButton = (
            <div>
                <Icon type="plus" />
                <div className="ant-upload-text">Upload</div>
            </div>
        );

        return (
            <Layout>
                <Loading />
                <AdminHeaderComponent />
                <Content>
                    <div id="admin-addstaff">
                        <div className="form">
                            <div className="title">
                                {this.state.id
                                    ? "แก้ไขบุคลากร"
                                    : "เพิ่มบุคลากร"}
                            </div>
                            <hr />
                            <Form>
                                <div id="title">ข้อมูลทั่วไป</div>
                                <FormItem
                                    label="รูปบุคลากร"
                                    {...formItemLayout}
                                >
                                    <div className="clearfix">
                                        <Upload
                                            accept="image/*"
                                            listType="picture-card"
                                            fileList={fileList}
                                            onPreview={this.handlePreview}
                                            onChange={this.handleChange}
                                        >
                                            {fileList.length >= 1
                                                ? null
                                                : uploadButton}
                                        </Upload>
                                        <Modal
                                            visible={previewVisible}
                                            footer={null}
                                            onCancel={this.handleCancel}
                                        >
                                            <img
                                                alt="example"
                                                style={{ width: "100%" }}
                                                src={previewImage}
                                            />
                                        </Modal>
                                    </div>
                                </FormItem>
                                <FormItem
                                    label="ชื่อ-นามสกุล"
                                    {...formItemLayout}
                                >
                                    <Input
                                        placeholder="รศ.ดร.จุลชีว สวัสดี"
                                        onChange={this.handleNameChange}
                                        value={this.state.name}
                                    />
                                </FormItem>
                                <FormItem
                                    label="Name (English)"
                                    {...formItemLayout}
                                >
                                    <Input
                                        placeholder="Asst.Prof.Julacheva Sawadee"
                                        onChange={this.handleNameEngChange}
                                        value={this.state.name_eng}
                                    />
                                </FormItem>
                                <FormItem
                                    label="ประเภทบุคลากร"
                                    {...formItemLayout}
                                >
                                    <Select
                                        showSearch
                                        placeholder="เลือกประเภทบุคลากร"
                                        onChange={this.handleTypeChange}
                                        value={this.state.type}
                                        // optionFilterProp=""
                                        // onChange={handleChange}
                                        // onFocus={handleFocus}
                                        // onBlur={handleBlur}
                                        filterOption={(input, option) =>
                                            option.props.children
                                                .toLowerCase()
                                                .indexOf(input.toLowerCase()) >=
                                            0
                                        }
                                    >
                                        <Option value="อาจารย์">อาจารย์</Option>
                                        <Option value="เจ้าหน้าที่">
                                            เจ้าหน้าที่
                                        </Option>
                                    </Select>
                                </FormItem>
                                <FormItem label="ตำแหน่ง" {...formItemLayout}>
                                    <Select
                                        showSearch
                                        placeholder="เลือกตำแหน่ง"
                                        onChange={this.handlePositionChange}
                                        value={this.state.position}
                                        // optionFilterProp=""
                                        // onChange={handleChange}
                                        // onFocus={handleFocus}
                                        // onBlur={handleBlur}
                                        filterOption={(input, option) =>
                                            option.props.children
                                                .toLowerCase()
                                                .indexOf(input.toLowerCase()) >=
                                            0
                                        }
                                    >
                                        <Option value="หัวหน้าภาควิชา">
                                            หัวหน้าภาควิชา
                                        </Option>
                                        <Option value="รองหัวหน้าภาควิชา">
                                            รองหัวหน้าภาควิชา
                                        </Option>
                                        <Option value="อาจารย์">อาจารย์</Option>
                                        <Option value="นักวิจัย">
                                            นักวิจัย
                                        </Option>
                                        <Option value="หัวหน้าห้องปฏิบัติการ">
                                        หัวหน้าห้องปฏิบัติการ
                                        </Option>
                                        <Option value="เจ้าหน้าที่">
                                            เจ้าหน้าที่
                                        </Option>
                                    </Select>
                                </FormItem>
                                <FormItem
                                    label="เบอร์โทรศัพท์"
                                    {...formItemLayout}
                                >
                                    <Input
                                        placeholder="080-000-0000"
                                        onChange={this.handleTelChange}
                                        value={this.state.tel}
                                    />
                                </FormItem>
                                <FormItem label="อีเมล" {...formItemLayout}>
                                    <Input
                                        placeholder="aaa@gmail.com"
                                        onChange={this.handleEmailChange}
                                        value={this.state.email}
                                    />
                                </FormItem>
                                <FormItem
                                    label="ข้อมูลการศึกษา"
                                    {...formItemLayout}
                                >
                                    <TextArea
                                        rows={4}
                                        onChange={this.handleEduDetailChange}
                                        value={this.state.edu_detail}
                                        placeholder="- Ph.D. (Food Science), Cornell University 1996 
                                    - M.S.(Biotechnology), Kasetsart University, Bangkok,Thailand. 1990 
                                    - B.Sc. (Radiological Technology), Mahidol University, Bangkok, Thailand. 1985"
                                    />
                                </FormItem>
                                <FormItem
                                    label="ประสบการณ์ทำงาน"
                                    {...formItemLayout}
                                >
                                    <TextArea
                                        rows={4}
                                        onChange={this.handleExpDetailChange}
                                        value={this.state.exp_detail}
                                        placeholder="- 1999-2018 Assistant Prof., King Mongkut’s University of Technology Thonburi
                                        - 1997-1999 Lecturer, King Mongkut’s University of Technology Thonburi"
                                    />
                                </FormItem>
                                <div id="title">ข้อมูลงานวิจัย</div>
                                <FormItem
                                    label="ขอบเขตงานวิจัย"
                                    {...formItemLayout}
                                >
                                    <TextArea
                                        rows={4}
                                        onChange={
                                            this.handleResearchScopeChange
                                        }
                                        value={this.state.research_scope}
                                        placeholder="- Microbial and Food Fermentation, Enzyme Technology"
                                    />
                                </FormItem>
                                <FormItem
                                    label="งานวิจัยที่ผ่านมา"
                                    {...formItemLayout}
                                >
                                    <TextArea
                                        rows={4}
                                        onChange={this.handleResearchDoneChange}
                                        value={this.state.research_done}
                                        placeholder="- Developed bacterial inoculum for cleaning up shrimp farm"
                                    />
                                </FormItem>
                                <FormItem
                                    label="งานวิจัยที่กำลังศึกษา"
                                    {...formItemLayout}
                                >
                                    <TextArea
                                        rows={4}
                                        onChange={
                                            this.handleResearchDoingChange
                                        }
                                        value={this.state.research_doing}
                                        placeholder="- Production of biopigments/food colorant from sponges-associated bacteria"
                                    />
                                </FormItem>
                                <FormItem
                                    label="งานวิจัยที่ตีพิมพ์"
                                    {...formItemLayout}
                                >
                                    <TextArea
                                        rows={4}
                                        onChange={
                                            this.handleResearchPublishChange
                                        }
                                        value={this.state.research_publish}
                                        placeholder="- Phengnuam, T. and W. Suntornsuk (2012) Detoxification of J. curcas seed cake by Bacillus fermentation. J. Biosci. Bioeng. (accepted) "
                                    />
                                </FormItem>
                                <FormItem
                                    label="รางวัลที่ได้รับ"
                                    {...formItemLayout}
                                >
                                    <TextArea
                                        rows={4}
                                        onChange={this.handleAwardChange}
                                        value={this.state.award}
                                        placeholder="- Royal Thai Government Scholarship for Ph.D. studies (1991-1996) "
                                    />
                                </FormItem>
                            </Form>
                            <Button
                                loading={this.state.loading}
                                className="form-button"
                                type="primary"
                                onClick={this.handleSubmit}
                            >
                                {this.state.id
                                    ? "บันทึกการแก้ไข"
                                    : "เพิ่มบุคลากร"}
                            </Button>
                        </div>
                    </div>
                </Content>
                <FooterComponent />
            </Layout>
        );
    }
}

export default AdminAddStaffPage;
