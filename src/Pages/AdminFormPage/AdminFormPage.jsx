import React, { Component } from "react";
import { Layout } from "antd";
import { Table } from "antd";
import { Row, Button, Modal, message } from "antd";
import pdf from "../../assets/pdf.png";
import close from "../../assets/cancel.svg";
import AdminHeaderComponent from "../../Component/AdminHeaderComponent/AdminHeaderComponent";
import FooterComponent from "../../Component/FooterComponent/FooterComponent";
import Loading from "../../Component/LoadingComponent/Loading";

const { Content } = Layout;
const confirm = Modal.confirm;

const columns = [
    {
        title: "ฟอร์ม",
        dataIndex: "name",
        key: "name",
        width: 350,
        render: text => <a href={`http://science.kmutt.ac.th/mic/api/${text.link}`}>{text.name}</a>
    },
    {
        title: "ดาวน์โหลด",
        dataIndex: "link",
        key: "action",
        width: 120,
        render: (text) => (
            <Row>
                <div className="icon">
                    <a href={`http://science.kmutt.ac.th/mic/api/${text}`}>
                        <img
                            src={pdf}
                            alt="pdf"
                            width="20px"
                            height="20px"
                        />
                    </a>
                </div>
            </Row>
        )
    },
    {
        title: "ลบไฟล์",
        dataIndex: "key",
        key: "key",
        width: 100,
        render: (text) => (
            <Row>
                <div className="icon">
                    <a onClick={() => {
                        confirm({
                            title: 'ต้องการลบไฟล์ ใช่หรือไม่?',
                            content: 'หากลบไปแล้วจะไม่สามารถนำกลับมาได้',
                            okText: 'ใช่',
                            okType: 'danger',
                            cancelText: 'ยกเลิก',
                            onOk() {
                                const fd = new FormData();
                                fd.append("id", text);
                                fetch("http://202.44.11.244/mic/api/?m=delete_form", {
                                    method: "POST",
                                    body: fd
                                })
                                    .then(resDel => resDel.json())
                                    .then(resDelJson => {
                                        if (!resDelJson.is_error) {
                                            message.success("ลบแบบฟอร์มสำเร็จ");
                                            setTimeout(() => {
                                                window.location.reload();
                                            }, 1500)
                                        }
                                    })
                                    .catch(err => {
                                        console.error(err);
                                        message.error("เกิดข้อผิดพลาดในการเชื่อมต่ออินเตอร์เน็ต");
                                    });


                            },
                            onCancel() {
                            },
                        });
                    }
                    }>
                        <img
                            src={close}
                            alt="close"
                            width="20px"
                            height="20px"
                        />
                    </a>

                </div>
            </Row>
        )
    }
];

class AdminFormPage extends Component {
    state = {
        data: []
    }

    componentDidMount() {
        fetch("http://202.44.11.244/mic/api/?m=load_form_list")
            .then(res => res.json())
            .then(resJson => {
                if (resJson.list) {
                    resJson.list.forEach(element => {
                        this.setState({
                            data: [...this.state.data, {
                                key: element.field.id,
                                name: {
                                    name: element.field.name,
                                    link: element.field.path
                                },
                                link: element.field.path
                            }]
                        })
                    });
                }
            })
    }

    showDeleteConfirm(key) {
        confirm({
            title: 'Are you sure delete this task?',
            content: 'Some descriptions',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
            },
            onCancel() {
            },
        });
    }

    navToAddForm = () => {
        this.props.history.push('/admin-addform')
    }

    render() {
        return (
            <Layout>
                <AdminHeaderComponent />
                <Content>
                    <div id="admin-addform">
                        <div className="form">
                            <div className="title title-form">
                                <div>จัดการแบบฟอร์ม</div>
                                <Button onClick={this.navToAddForm} type="primary">เพิ่มแบบฟอร์ม</Button>
                            </div>
                            <hr />
                            <div id="form-component">
                                <Table columns={columns} dataSource={this.state.data} />
                            </div>
                        </div>
                    </div>
                </Content>
                <FooterComponent />
            </Layout>

        );
    }
}

export default AdminFormPage;
