import React, { Component } from "react";
import "./AdminLoginPage.css";
import { Form, Icon, Input, Button, Checkbox, Alert, message } from "antd";
import * as firebase from "firebase";
import Loading from "../../Component/LoadingComponent/Loading";

const FormItem = Form.Item;

class AdminLoginPage extends React.Component {
    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            error: false,
            loading: false,
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                email: "",
                password: "",
                error: false,
                loading: false,
            })
        }, 500)
    }

    onChangeEmail = e => {
        this.setState({
            email: e.target.value,
            password: this.state.password,
            error: this.state.error,
            loading: false,
        });
    };

    onChangePassword = e => {
        this.setState({
            email: this.state.email,
            password: e.target.value,
            error: this.state.error,
            loading: false,
        });
    };

    onClickForgotPassword = e => {
        firebase.auth().sendPasswordResetEmail(
            // "kijpokin.nga@hotmail.com"
        ).then(res => {
            // message.info()
        }).catch(error => {

        })
    }

    handleSubmit = e => {
        this.setState({
            email: this.state.email,
            password: this.state.password,
            error: this.state.error,
            loading: true
        }, () => {
            firebase
                .auth()
                .signInWithEmailAndPassword(this.state.email, this.state.password)
                .then(() => {
                    message.success("เข้าสู่ระบบ")
                    this.props.history.push("/admin-form")
                    this.setState({
                        email: this.state.email,
                        password: this.state.password,
                        error: this.state.error,
                        loading: false,
                    });
                })
                .catch(error => {
                    message.error("อีเมลผู้ใช้งาน หรือ รหัสผ่าน ไม่ถูกต้อง")
                    this.setState({
                        email: this.state.email,
                        password: this.state.password,
                        error: true,
                        loading: false,
                    });
                });
        });

        e.preventDefault();

    };

    render() {
        const { email, password, error, loading } = this.state;

        return (
            <div className="bg-color">
                <Loading />
                <div className="login-box">
                    <div className="head-login"> Administrator Login </div>
                    <hr />
                    <div className="content-login">
                        <Form
                            onSubmit={this.handleSubmit}
                            className="login-form"
                        >
                            <div className="label"> อีเมลผู้ใช้งาน : </div>
                            <FormItem>
                                {
                                    <Input
                                        onChange={this.onChangeEmail}
                                        prefix={
                                            <Icon
                                                type="mail"
                                                style={{
                                                    color: "rgba(0,0,0,.25)"
                                                }}
                                            />
                                        }
                                        placeholder="อีเมล"
                                        value={email}
                                    />
                                }
                            </FormItem>
                            <div className="label"> รหัสผ่าน : </div>
                            <FormItem>
                                {
                                    <Input
                                        onChange={this.onChangePassword}
                                        prefix={
                                            <Icon
                                                type="lock"
                                                style={{
                                                    color: "rgba(0,0,0,.25)"
                                                }}
                                            />
                                        }
                                        type="password"
                                        placeholder="รหัสผ่าน"
                                        value={password}
                                    />
                                }
                            </FormItem>
                            <Button
                                type="primary"
                                htmlType="submit"
                                className="login-form-button"
                                disabled={
                                    email !== "" && password !== ""
                                        ? false
                                        : true
                                }
                                loading={loading}
                            >
                                เข้าสู่ระบบ
                            </Button>
                        </Form>
                    </div>
                </div>
            </div>
        );
    }
}

export default AdminLoginPage;
