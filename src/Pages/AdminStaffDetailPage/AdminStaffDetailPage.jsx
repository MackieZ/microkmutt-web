import React from "react";
import { Layout } from "antd";
import StaffDetailComponent from "../../Component/StaffDetailComponent/StaffDetailComponent";
import AdminHeaderComponent from "../../Component/AdminHeaderComponent/AdminHeaderComponent";
import FooterComponent from "../../Component/FooterComponent/FooterComponent";
import Loading from "../../Component/LoadingComponent/Loading";

const { Content } = Layout;
// const SubMenu = Menu.SubMenu;
// const MenuItemGroup = Menu.ItemGroup;

class AdminStaffDetailPage extends React.Component {

    render() {
        return (
            <Layout>
                <Loading />
                <AdminHeaderComponent />
                <div className="pad-before-content" />
                <Content>
                    <div>
                        <StaffDetailComponent id={`${this.props.match.params.id}`}/>
                    </div>
                </Content>
                <FooterComponent />
            </Layout>
        );
    }
}

export default AdminStaffDetailPage;
