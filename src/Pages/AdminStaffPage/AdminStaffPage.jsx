import React, { Component } from "react";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import { Layout, Menu } from "antd";
import { Row, Col, Button, Modal, message } from "antd";
import AdminStaffComponent from "../../Component/AdminStaffComponent/AdminStaffComponent";
import AdminHeaderComponent from "../../Component/AdminHeaderComponent/AdminHeaderComponent";
import SubHeaderComponent from "../../Component/SubHeaderComponent/SubHeaderComponent";
import FooterComponent from "../../Component/FooterComponent/FooterComponent";
import Loading from "../../Component/LoadingComponent/Loading";
import "./AdminStaffPage.css";

const { Header, Content, Footer } = Layout;

class AdminStaffPage extends React.Component {
    navToAddStaff = () => {
        this.props.history.push("/admin-addstaff");
    };

    render() {
        return (
            <Layout>
                <Loading />
                <AdminHeaderComponent />
                <Content>
                    <div id="admin-staff">
                        <div className="form">
                            <div className="title title-staff">
                                จัดการบุคลากร
                                <Button
                                    onClick={this.navToAddStaff}
                                    type="primary"
                                >
                                    เพิ่มบุคลากร
                                </Button>
                            </div>
                            <hr />
                        </div>
                    </div>

                    <div>
                        <AdminStaffComponent />
                    </div>
                </Content>
                <FooterComponent />
            </Layout>
        );
    }
}

export default AdminStaffPage;
