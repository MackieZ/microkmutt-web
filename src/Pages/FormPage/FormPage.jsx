import React from "react";
import { Layout } from "antd";
import FormComponent from "../../Component/FormComponent/FormComponent";
import HeaderComponent from "../../Component/HeaderComponent/HeaderComponent";
import SubHeaderComponent from "../../Component/SubHeaderComponent/SubHeaderComponent";
import FooterComponent from "../../Component/FooterComponent/FooterComponent";
import Loading from "../../Component/LoadingComponent/Loading";

const { Content } = Layout;

class FormPage extends React.Component {
    render() {
        return (
            <Layout>
                <Loading />
                <HeaderComponent />
                <SubHeaderComponent title="แบบฟอร์มต่างๆ"/>
                <Content>
                    <div>
                        <FormComponent />
                    </div>
                </Content>
                <FooterComponent />
            </Layout>
        );
    }
}

export default FormPage;
