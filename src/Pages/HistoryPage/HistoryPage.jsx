import React from "react";
import { Layout } from "antd";
import HistoryComponent from "../../Component/HistoryComponent/HistoryComponent";
import HeaderComponent from "../../Component/HeaderComponent/HeaderComponent";
import SubHeaderComponent from "../../Component/SubHeaderComponent/SubHeaderComponent";
import FooterComponent from "../../Component/FooterComponent/FooterComponent";
import Loading from "../../Component/LoadingComponent/Loading";

const { Content } = Layout;

class HistoryPage extends React.Component {
    render() {
        return (
            <Layout>
                <Loading />
                <HeaderComponent />
                <SubHeaderComponent title="ประวัติภาควิชา"/>
                <div className="pad-before-content" />
                <Content>
                    <div>
                        <HistoryComponent />
                    </div>
                </Content>
                <FooterComponent />
            </Layout>
        );
    }
}

export default HistoryPage;
