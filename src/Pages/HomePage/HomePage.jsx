import React, { Component } from "react";
import { BrowserRouter as Router, Link, Route, Switch, Redirect } from "react-router-dom";
import { Layout, Menu } from "antd";
import HeaderComponent from "../../Component/HeaderComponent/HeaderComponent";
import SubHeaderComponent from "../../Component/SubHeaderComponent/SubHeaderComponent";
import FooterComponent from "../../Component/FooterComponent/FooterComponent";
import Loading from "../../Component/LoadingComponent/Loading";

const { Header, Content, Footer } = Layout;
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class HomePage extends React.Component {
    constructor() {
        super();

    }
    
    render() {
        return (
            <Redirect to='/staff' />
            
            // <Layout>
            //     <Loading />
            //     <HeaderComponent />
            //     <SubHeaderComponent title="Home"/>
            //     <div className="pad-before-content" />
            //     <Content>
            //         <div
            //             style={{
            //                 background: "#fff",
            //                 padding: 24,
            //                 minHeight: 380
            //             }}
            //         >
            //             asd
            //         </div>
            //     </Content>
            //     <FooterComponent />
            // </Layout>
        );
    }
}

export default HomePage;
