import React from "react";
import "./ProgramPage.css";
import HeaderComponent from "../../Component/HeaderComponent/HeaderComponent";
import FooterComponent from "../../Component/FooterComponent/FooterComponent";
import { Layout, Row, Col, Button } from "antd";
import Loading from "../../Component/LoadingComponent/Loading";
import SubHeaderComponent from "../../Component/SubHeaderComponent/SubHeaderComponent";
import foodsci from "../../assets/foodsci-program.jpg";
import micro from "../../assets/micro-program.jpeg";
import master from "../../assets/master-program.jpeg";
import dna from "../../assets/dna.png";

const { Content } = Layout;
class ProgramPage extends React.Component {
    render() {
        return (
            <Layout id="program">
                <Loading />
                <HeaderComponent />
                <SubHeaderComponent title="หลักสูตร"/>
                <Content>
                    <Row className="full-width-content bg-1">
                        <Col span={24} md={12} className="program-img">
                            <img src={micro} />
                        </Col>
                        <Col span={24} md={12} className="program-content bg-1">
                            <Row>
                                <Row className="program-header-title primary-color-text">
                                    หลักสูตรวิทยาศาสตรบัณฑิต
                                </Row>
                                <Row className="program-subheader-title">
                                    สาขาวิชาจุลชีววิทยา
                                </Row>
                                <Row className="program-button">
                                    <Button size="large">
                                        รายละเอียดเพิ่มเติม
                                    </Button>
                                </Row>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="full-width-content bg-2 program-web">
                        <Col span={24} md={12} className="program-content bg-2">
                            <Row>
                                <Row className="program-header-title primary-color-text">
                                    หลักสูตรวิทยาศาสตรบัณฑิต
                                </Row>
                                <Row className="program-subheader-title">
                                    สาขาวิชาวิทยาศาสตร์และเทคโนโลยีการอาหาร
                                </Row>
                                <Row className="program-button">
                                    <Button size="large">
                                        รายละเอียดเพิ่มเติม
                                    </Button>
                                </Row>
                            </Row>
                        </Col>
                        <Col span={24} md={12} className="program-img">
                            <img src={foodsci} />
                        </Col>
                    </Row>
                    <Row className="full-width-content bg-2 program-mobile">
                        <Col span={24} md={12} className="program-img">
                            <img src={foodsci} />
                        </Col>
                        <Col span={24} md={12} className="program-content bg-2">
                            <Row>
                                <Row className="program-header-title primary-color-text">
                                    หลักสูตรวิทยาศาสตรบัณฑิต
                                </Row>
                                <Row className="program-subheader-title">
                                    สาขาวิชาวิทยาศาสตร์และเทคโนโลยีการอาหาร
                                </Row>
                                <Row className="program-button">
                                    <Button size="large">
                                        รายละเอียดเพิ่มเติม
                                    </Button>
                                </Row>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="full-width-content bg-1">
                        <Col span={24} md={12} className="program-img">
                            <img src={master} />
                        </Col>
                        <Col span={24} md={12} className="program-content bg-1">
                            <Row>
                                <Row className="program-header-title primary-color-text">
                                    หลักสูตรวิทยาศาสตรมหาบัณฑิต
                                </Row>
                                <Row className="program-subheader-title">
                                    สาขาวิชาจุลชีววิทยาประยุกต์
                                </Row>
                                <Row className="program-button">
                                    <Button size="large">
                                        รายละเอียดเพิ่มเติม
                                    </Button>
                                </Row>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="full-width-content bg-2 program-web">
                        <Col span={24} md={12} className="program-content bg-2">
                            <Row>
                                <Row className="program-header-title primary-color-text">
                                    หลักสูตรปรัชญาดุษฎีบัณฑิต
                                </Row>
                                <Row className="program-subheader-title">
                                    สาขาวิชาวิทยาศาสตร์ชีวภาพ (หลักสูตรนานาชาติ)
                                </Row>
                                <Row className="program-button">
                                    <Button size="large">
                                        รายละเอียดเพิ่มเติม
                                    </Button>
                                </Row>
                            </Row>
                        </Col>
                        <Col span={24} md={12} className="program-img">
                            <img src={dna} />
                        </Col>
                    </Row>
                    <Row className="full-width-content bg-2 program-mobile">
                        <Col span={24} md={12} className="program-img">
                            <img src={dna} />
                        </Col>
                        <Col span={24} md={12} className="program-content bg-2">
                            <Row>
                                <Row className="program-header-title primary-color-text">
                                    หลักสูตรปรัชญาดุษฎีบัณฑิต
                                </Row>
                                <Row className="program-subheader-title">
                                    สาขาวิชาวิทยาศาสตร์ชีวภาพ (หลักสูตรนานาชาติ)
                                </Row>
                                <Row className="program-button">
                                    <Button size="large">
                                        รายละเอียดเพิ่มเติม
                                    </Button>
                                </Row>
                            </Row>
                        </Col>
                    </Row>
                </Content>
                <FooterComponent />
            </Layout>
        );
    }
}

export default ProgramPage;
