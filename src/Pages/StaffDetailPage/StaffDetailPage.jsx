import React, { Component } from "react";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import { Layout, Menu } from "antd";
import StaffDetailComponent from "../../Component/StaffDetailComponent/StaffDetailComponent";
import HeaderComponent from "../../Component/HeaderComponent/HeaderComponent";
import SubHeaderComponent from "../../Component/SubHeaderComponent/SubHeaderComponent";
import FooterComponent from "../../Component/FooterComponent/FooterComponent";
import Loading from "../../Component/LoadingComponent/Loading";

const { Header, Content, Footer } = Layout;
// const SubMenu = Menu.SubMenu;
// const MenuItemGroup = Menu.ItemGroup;

class StaffPage extends React.Component {

    render() {
        return (
            <Layout>
                <Loading />
                <HeaderComponent />
                <SubHeaderComponent title="บุคลากร"/>
                <div className="pad-before-content" />
                <Content>
                    <div>
                        <StaffDetailComponent id={`${this.props.match.params.id}`}/>
                    </div>
                </Content>
                <FooterComponent />
            </Layout>
        );
    }
}

export default StaffPage;
